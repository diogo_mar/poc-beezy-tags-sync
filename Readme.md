# PoC - Beezy tags sync

When a Beezy story or page is created, the list entry in SharePoint does not include the tags (but the column exists from the custom content types).
This Azure Function gets the tags for recently edited or created stories/pages and adds them to the SharePoint list so the information can be indexed and refined by SharePoint search. 

## Pre-requisites

- Beezy installed and administrator role in the resource group of the installation.
- Install [Visual Studio 2019](https://visualstudio.microsoft.com/) with [.NET Core 3.1 SDK](https://www.microsoft.com/net/download/core) on your development computer.

## Configure Azure resources

###  Create azure app registration
1. Follow the steps from [this guide](https://docs.microsoft.com/en-us/graph/connecting-external-content-build-quickstart?tutorial-step=1). 
   - The registration should be done in the same Azure Active Directory as the Beezy Installation. 
   - You can give a different name to the application in step 4
2. Copy and save the **application id**, **tenant id** and the **secret value** into a text file for later use
  
### Configure database authentication
1. Follow these steps to [Provision Azure AD admin](https://docs.microsoft.com/en-us/azure/azure-sql/database/authentication-aad-configure?tabs=azure-powershell#provision-azure-ad-admin-sql-managed-instance) in the sql server
2. Open the Beezy SQL Database from the [Azure Portal](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.Sql%2Fservers%2Fdatabases)
3. Take the oportunity to copy the **Server name** value and store it for later use
4. Open the Query Editor and login using the Active Directory authentication (make sure you're logged in as the admin account you configured in step 1)
5. Open a new Query and run the following command to authenticate the app:
   ```sql
   CREATE USER [app name] FROM EXTERNAL PROVIDER
   ```
   - replace [app name] with the name given to the app registration in the previous section
6. Run the following query to give the app read permissions
   ```sql 
   ALTER ROLE db_datareader ADD MEMBER [app name]
   ```

## Configure project

1. Open the project in Visual Studio 2019
2. If the `local.setting.json` file already exists, open it and skip to step 5
3. Right click the `BeezyTagsSync` project and click `Add -> New item...`
4. Under **General** select **Text File** and name it `local.setting.json`
5. Paste the following content on the file:
    ```json
    {
        "IsEncrypted": false,
        "Values": {
            "appId": "<YOUR_APP_ID_HERE>",
            "tenantId": "<YOUR_TENANT_ID_HERE>",
            "appSecret": "<YOUR_APP_SECRET_HERE>",
            "serverName": "<SERVER_URL_OF_THE_DATABASE>",
            "hostName": "<YOUR_TENANT_NAME>.sharepoint.com",
            "AzureWebJobsStorage": "UseDevelopmentStorage=true",
            "FUNCTIONS_WORKER_RUNTIME": "dotnet"
        }
    }
    ```
6. Save the changes. You can now test the project locally.

## Runing the project locally

1. Open the project in Visual Studio 2019
2. Click the play button in the toolbar to start debugging.
3. The azure function will run once and then again every hour at minute 5
4. Close the console and debug again to rerun the azure function at any time.

## Runing the Azure Function in Azure

### Publish the function 

1. Right click the `BeezyTagsSync` project and click `Publish...`
2. If no prompt appears, click the **+ Add**  button
3. In the publishing wizard, select **Azure** for the target
4. In the following step choose **Azure Funtion App (Windows)**
5. Select your subscription (the same where Beezy is installed)
6. Under **Function Apps**, select and existing function or click the **+** button to create a new one
7. For the Deployment type, select **Publish (generates pubxml file)**
8. Click **OK** to close the wizard. The publishing page will now appear.
9. You can now use the Publish button to publish the azure function to azure
10. Open the Azure Portal and find and open your Azure Function
11. Under **Settings**, click on **Configuration**
12. Click **+ New applicaiton setting** and fill the form for each of the following values
    ```
    "appId": "<YOUR_APP_ID_HERE>",
    "tenantId": "<YOUR_TENANT_ID_HERE>",
    "appSecret": "<YOUR_APP_SECRET_HERE>",
    "serverName": "<SERVER_URL_OF_THE_DATABASE>",
    "hostName": "<YOUR_TENANT_NAME>.sharepoint.com",
    ```
13. When done, click save. Your azure function will now run every hour on the 5th minute.

### Publishing changes

1. Right click the `BeezyTagsSync` project and click `Publish...`
2. If not selected, select the Azure Function profile at the top of the page
3. Click the **Publish** button