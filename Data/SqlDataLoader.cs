﻿using BeezyTagsSync.Authentication;
using BeezyTagsSync.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BeezyTagsSync.Data
{
    public static class SqlDataLoader
    {

        async public static Task<List<BeezyActivity>> LoadActivitiesFromDbAsync(ClientCredentialAuthProvider authProvider, DateTime modifiedAfter, string serverUrl, string database = "beezy")
        {
            List<BeezyActivity> activities = new List<BeezyActivity>();
            try
            {
                SqlConnection connection = await BuildSqlConnection(authProvider, serverUrl, database);
                string sqlCommand = BuildSqlQuery(modifiedAfter);

                connection.Open();
                using (SqlCommand command = new SqlCommand(sqlCommand, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var tags = reader.IsDBNull(6) ? new List<string>() : new List<string>(reader.GetString(6).Split(','));

                            activities.Add(new BeezyActivity
                            {
                                Id = reader.GetInt32(0),
                                Type = reader.GetString(1),
                                ListGuid = reader.GetString(2),
                                ItemId = reader.GetInt32(3),
                                Url = reader.GetString(4),
                                LastModified = reader.GetDateTime(5),
                                HashTags = tags
                            });
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return activities;
        }

        async static Task<SqlConnection> BuildSqlConnection(ClientCredentialAuthProvider authProvider, string serverUrl, string database = "beezy")
        {
            string connectionString = String.Format("Server= tcp:{0},1433; Initial Catalog = {1}; Persist Security Info = False; MultipleActiveResultSets = False; Encrypt = True; TrustServerCertificate = False;", serverUrl, database);
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.InitialCatalog = database;
            builder.TrustServerCertificate = false;
            builder.Encrypt = true;
            builder.MultipleActiveResultSets = false;
            builder.PersistSecurityInfo = false;
            builder.DataSource = String.Format("tcp:{0},1433", serverUrl);

            SqlConnection connection = new SqlConnection(builder.ConnectionString);
            await authProvider.AuthenticateConnectionAsync(connection);

            return connection;
        }

        static string BuildSqlQuery(DateTime modifiedAfter)
        {
            string sqlCommandRaw =
                "SELECT" +
                "   activity.Id, activity.type, activity.ListGuid, activity.ItemId, activity.Url, activity.LastModified," +
                "   (" +
                "       SELECT STRING_AGG('-1;#' + hashTag.Name + '|' +lower(convert(nvarchar(38),hashTag.MetadataId)), ',')" +
                "       FROM [dbo].[HashTag] hashTag" +
                "       LEFT JOIN [dbo].[HashTaggedActivity] hashtagActivity ON hashTag.id = hashtagActivity.HashTag_id" +
                "       WHERE hashtagActivity.Activity_id = activity.id " +
                "   ) AS HashTags " +
                "FROM [dbo].[Activity] activity " +
                "WHERE Type IN ('story', 'corporatepage') " +
                "   AND Visibility=126 " +
                "   AND Activity.LastModified >= '{0}'";


            // Dates before the year 1753 are outside the bounds for sql date
            var boundDate = modifiedAfter.Year >= 1753 ? modifiedAfter : modifiedAfter.AddYears(1753 - modifiedAfter.Year);

            return String.Format(sqlCommandRaw, boundDate.ToString("yyyy-MM-dd HH:mm:ss.fff"));
        }
    }
}
