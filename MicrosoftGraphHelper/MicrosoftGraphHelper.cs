﻿using BeezyTagsSync.Models;
using Microsoft.Graph;
using Microsoft.Graph.ExternalConnectors;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BeezyTagsSync.MicrosoftGraph
{
    public class MicrosoftGraphHelper
    {
        private GraphServiceClient _microsoftGraphClient;

        public MicrosoftGraphHelper(IAuthenticationProvider authProvider)
        {
            var httpProvider = new HttpProvider();

            // Initialize the Microsoft Graph client
            _microsoftGraphClient = new GraphServiceClient(authProvider, httpProvider);
        }


        public async Task<string> GetSiteId(string hostName, string siteUrl)
        {
            var id = String.Format("{0}:/{1}", hostName, siteUrl);
            var site = await _microsoftGraphClient.Sites[id].Request().GetAsync();

            return site.Id;
        }

        public async Task<List<string>> GetListItemTags(string siteId, string listGuid, int itemId)
        {
            ListItem item = await _microsoftGraphClient.Sites[siteId].Lists[listGuid].Items[itemId.ToString()].Request().GetAsync();
            List<string> tags = new List<string>();

            var tagsField = item.Fields.AdditionalData["HashTag"];
            var tagsList = JsonConvert.DeserializeObject<List<SharePointTerm>>(tagsField.ToString());

            return tagsList.ConvertAll<string>((SharePointTerm input) => input.AsString());
        }

        public async Task<string> getHiddenTermsetColumnName(string siteId, string listGuid)
        {
            var columns = await _microsoftGraphClient.Sites[siteId].Lists[listGuid].Columns.Request(new[] { new QueryOption("expand", "hidden") }).Select("displayName,name").GetAsync();
            foreach (var column in columns)
            {
                if (column.DisplayName == "HashTag_0")
                {
                    return column.Name;
                }
            }

            throw new Exception(String.Format("HashTag_0 Column not found in list ${0}", listGuid));
        }

        public async Task SetListItemTags(string siteId, string listGuid, int itemId, List<string> tags)
        {
            var hashtagColumnname = await getHiddenTermsetColumnName(siteId, listGuid);
            var fieldValueSet = new FieldValueSet
            {
                AdditionalData = new Dictionary<string, object>()
                {
                    { hashtagColumnname, String.Join(';',tags) }
                }
            };

            var response = await _microsoftGraphClient.Sites[siteId].Lists[listGuid].Items[itemId.ToString()].Fields.Request().UpdateAsync(fieldValueSet);
        }
    }
}
