using System;
using BeezyTagsSync.MicrosoftGraph;
using BeezyTagsSync.Authentication;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using BeezyTagsSync.Data;
using System.Threading.Tasks;

namespace BeezyTagsSync
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task Run([TimerTrigger("0 5 * * * *", RunOnStartup =true)] TimerInfo myTimer, ILogger log)
        {
            var lastRunTime = myTimer.ScheduleStatus.Last;

            var appId = Environment.GetEnvironmentVariable("appId");
            var tenantId = Environment.GetEnvironmentVariable("tenantId");
            var appSecret = Environment.GetEnvironmentVariable("appSecret");
            var serverName = Environment.GetEnvironmentVariable("serverName");
            var hostName = Environment.GetEnvironmentVariable("hostName"); // i.e: contoso.sharepoint.com
            
            var authProvider = new ClientCredentialAuthProvider(appId, tenantId, appSecret);
            var microsoftGraphHelper = new MicrosoftGraphHelper(authProvider);

            var activities = await SqlDataLoader.LoadActivitiesFromDbAsync(authProvider, lastRunTime, serverName);

            log.LogInformation("{0} activities found", activities.Count);
            foreach (var activity in activities)
            {
                var siteId = await microsoftGraphHelper.GetSiteId(hostName, activity.SiteUrl);
                var storedTags = await microsoftGraphHelper.GetListItemTags(siteId, activity.ListGuid, activity.ItemId);
                var haveSameCount = activity.HashTags.Count == storedTags.Count;
                var haveSameTags = activity.HashTags.TrueForAll(tag => storedTags.Contains(tag));

                if(!haveSameTags || !haveSameCount)
                {
                    await microsoftGraphHelper.SetListItemTags(siteId, activity.ListGuid, activity.ItemId, activity.HashTags);
                }
            }
        }
    }
}
