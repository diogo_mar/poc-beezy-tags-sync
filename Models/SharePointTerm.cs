﻿using System;

namespace BeezyTagsSync.Models
{
    public class SharePointTerm
    {
        public string Label { get; set; }
        public string TermGuid { get; set; }

        public string AsString()
        {
            return String.Format("-1;#{0}|{1}", Label, TermGuid.ToLower());
        }
    }
}