﻿using Microsoft.Graph.ExternalConnectors;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeezyTagsSync.Models
{
    public class BeezyActivity
    {
        // activity.Id, activity.ListGuid, activity.ItemId, activity.Url, activity.LastModified
        public int Id { get; set; }
        public string Type { get; set; }
        public string ListGuid { get; set; }
        public int ItemId { get; set; }
        public string Url { get; set; }
        public DateTime LastModified { get; set; }
        public List<string> HashTags { get; set; }

        public string SiteUrl
        {
            get
            {
                return this.Url.ToLower().Split("/pages")[0];
            }
        }
    }
}
